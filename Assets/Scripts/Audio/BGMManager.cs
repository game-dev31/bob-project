using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BGMManager : MonoBehaviour
{
    [SerializeField] private AudioSource bgmSource;
    [SerializeField] Image on;
    [SerializeField] Image off;
    
    [SerializeField] TextMeshProUGUI onText;
    [SerializeField] TextMeshProUGUI offText;
    
    private bool bgmMuted = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("bgmMuted"))
        {
            PlayerPrefs.SetInt("bgmMuted", 0);
        }
        else
        {
            Load();
        }
        
        UpdateButtonIcon();
        bgmSource.mute = bgmMuted;
        //AudioListener.pause = muted;
    }

    private void UpdateButtonIcon()
    {
        if (bgmMuted == false)
        {
            on.enabled = true;
            off.enabled = false;
            
            onText.enabled = true;
            offText.enabled = false;
        }
        else
        {
            on.enabled = false;
            off.enabled = true;
            
            onText.enabled = false;
            offText.enabled = true;
        }
    }
    
    public void OnButtonPress()
    {
        if (bgmMuted == false)
        {
            bgmMuted = true;
            bgmSource.mute = bgmMuted;
            //AudioListener.pause = true;
        }
        else
        {
            bgmMuted = false;
            bgmSource.mute = bgmMuted;
            //AudioListener.pause = false;
        }
        
        Save();
        UpdateButtonIcon();
    }

    private void Load()
    {
        bgmMuted = PlayerPrefs.GetInt("bgmMuted") == 1;
    }

    private void Save()
    {
        PlayerPrefs.SetInt("bgmMuted", bgmMuted ? 1 : 0);
    }
}

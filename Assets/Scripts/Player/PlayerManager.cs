using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerManager : MonoBehaviour
{
    public bool isActive = false;
    
    // Moving
    [SerializeField] private float speed;
    private Vector3 direction;
    // Animation
    [SerializeField] private bool isMoving = false;
    private Animator playerAnim;
    // Camera
    private Camera cam;
    // Money
    [SerializeField] private List<Transform> money = new List<Transform>();

    private BoxCollider bc;
    
    
    private Vector3 lastPosition;
    private float stuckTime = 0f;
    [SerializeField] private float stuckThreshold = 0.1f; 
    [SerializeField] private float stuckDuration = 1f; 

    void Start()
    {
        cam = Camera.main;
        playerAnim = GetComponent<Animator>();
        bc = GetComponent<BoxCollider>();
        lastPosition = transform.position;
        isActive = false;
    }

   void Update()
    {
        if (isActive == true)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                isMoving = false;
                playerAnim.SetBool("IsWalk", isMoving);
                return;
            }

            // Player Movement
            if (Input.GetMouseButtonUp(0))
            {
                
                Plane plane = new Plane(Vector3.up, transform.position);
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

                if (plane.Raycast(ray, out var distance))
                {
                    
                    Vector3 targetPosition = ray.GetPoint(distance);
                    Vector3 directionToMove = targetPosition - transform.position;

                    // Check for buildings along the movement direction
                    if (!IsPathBlockedByBuilding(directionToMove))
                    {
                        speed = 4;
                        direction = targetPosition;
                        isMoving = true;

                        if (directionToMove.magnitude > 1f)
                        {
                            transform.LookAt(direction);
                        }
                    }
                    else
                    {
                        Debug.Log("Path is blocked by a building.");
                        isMoving = false;
                    }
                }
            }

            // Move player if not blocked
            if (isMoving)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                    new Vector3(direction.x, transform.position.y, direction.z), speed * Time.deltaTime);

                if (transform.position == new Vector3(direction.x, transform.position.y, direction.z))
                {
                    isMoving = false;
                }

                playerAnim.SetBool("IsWalk", isMoving);
            }

            if (money.Count > 0)
            {
                MoveMoney();
            }

            CheckForMoneyCollection();
            CheckForStuck();
        }
    }

    private bool IsPathBlockedByBuilding(Vector3 directionToMove)
    {
        Ray ray = new Ray(transform.position, directionToMove.normalized);
        if (Physics.Raycast(ray, out RaycastHit hit, directionToMove.magnitude))
        {
            if (hit.collider.CompareTag("Building"))
            {
                return true; // Path is blocked by a building
            }
        }
        return false; // Path is clear
    }


    private void CheckForStuck()
    {
        float distanceMoved = Vector3.Distance(transform.position, lastPosition);

        if (distanceMoved < stuckThreshold && isMoving)
        {
            stuckTime += Time.deltaTime;
            if (stuckTime >= stuckDuration)
            {
                
                isMoving = false;
                playerAnim.SetBool("IsWalk", isMoving);
                speed = 0;
                stuckTime = 0f;
            }
        }
        else
        {
            
            stuckTime = 0f;
        }
        
        lastPosition = transform.position;
    }

    private void MoveMoney()
    {
        if (money.Count > 0)
        {
            money[0].position = Vector3.MoveTowards(money[0].position, transform.position + (transform.forward * 0.5f), Time.deltaTime * 15f);

            for (int i = 1; i < money.Count; i++)
            {
                var firstMoney = money[i - 1];
                var secMoney = money[i];

                secMoney.position = Vector3.MoveTowards(secMoney.position,
                    new Vector3(firstMoney.position.x, firstMoney.position.y + 0.17f, firstMoney.position.z),
                    Time.deltaTime * speed * 15f);
            }

            DestroyMoneyItems();
        }
    }

    private void DestroyMoneyItems()
    {
        var moneyToDestroy = new List<Transform>();

        foreach (var item in money)
        {
            if (Vector3.Distance(item.position, transform.position) < 0.5f)
            {
                moneyToDestroy.Add(item);
            }
        }

        foreach (var item in moneyToDestroy)
        {
            money.Remove(item);
            Destroy(item.gameObject);
        }
    }

    private void CheckForMoneyCollection()
    {
        if (Physics.Raycast(transform.position, transform.forward, out var hit, 1f))
        {
            Debug.DrawRay(transform.position, transform.forward * 1f, Color.green);

            if (hit.collider.CompareTag("Table"))
            {
                if (hit.collider.transform.childCount > 2)
                {
                    var mn = hit.collider.transform.GetChild(1);
                    money.Add(mn);
                    mn.parent = null;

                    if (hit.collider.transform.parent.GetComponent<MoneyMaker>().countMoney > 1)
                    {
                        hit.collider.transform.parent.GetComponent<MoneyMaker>().countMoney--;
                    }

                    if (hit.collider.transform.parent.GetComponent<MoneyMaker>().currentYAxis > 0f)
                    {
                        hit.collider.transform.parent.GetComponent<MoneyMaker>().currentYAxis -= hit.collider.transform.parent.GetComponent<MoneyMaker>().YAxis;
                    }

                    Coinmanagement.singleton.collectCoin = true;
                    SFX.singleton.CollectCoin();
                }
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.forward * 1f, Color.red);
        }
    }

    public void activatePlayer()
    {
        isActive = true;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Coinmanagement : MonoBehaviour
{
    public static Coinmanagement singleton;
    
    [SerializeField] private float coin = 10f;
    public float keepCoin = 0f;
    public bool collectCoin = false;

    [SerializeField] private TextMeshProUGUI number;

    public bool showCoin = false;

    private void Awake()
    {
        singleton = this;
        Debug.Log("Coinmanagement Awake method called");
    }

    private void Start()
    {
        showCoin = false;
    }

    public float Coin
    {
        get { return coin; }
        set { coin = value; }
    }

    private void Update()
    {
        Debug.Log($"Update called. Current coin value: {coin}");

        if (number != null && showCoin == true)
        {
            number.text = coin.ToString();
            Debug.Log($"Updated text to: {number.text}");
        }

        if (coin < 0)
        {
            coin = 0;
        }

        if (collectCoin)
        {
            Coin += keepCoin;
            keepCoin = 0;
            collectCoin = false;
            Debug.Log($"Collected coins. New value: {coin}");
        }
    }

    public void ShowCoin()
    {
        showCoin = true;
    }
}
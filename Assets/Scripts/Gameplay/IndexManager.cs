using System.Collections.Generic;
using UnityEngine;

public class IndexManager : MonoBehaviour
{
    public static IndexManager Instance { get; private set; }

    //ItemUpgradeV3 Index
    [SerializeField] private int nextAvailableIndex = 0;
    private List<bool> itemStatus = new List<bool>();
    
    //ItemBehaviour Index
    [SerializeField] private int nextAvailableIndexForItemBehaviour = 0;
    private List<bool> itemBehaviourStatus = new List<bool>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public int GetNextAvailableIndex()
    {
        int index = nextAvailableIndex;
        nextAvailableIndex++;
        EnsureListSize(index);
        return index;
    }

    public bool IsItemBought(int index)
    {
        EnsureListSize(index);
        return itemStatus[index];
    }

    public void SetItemStatus(int index, bool status)
    {
        EnsureListSize(index);
        itemStatus[index] = status;
    }

    private void EnsureListSize(int index)
    {
        if (index >= itemStatus.Count)
        {
            itemStatus.AddRange(new bool[index - itemStatus.Count + 1]);
        }
    }
    
    
    
    public int _GetNextAvailableIndex()
    {
        int index = nextAvailableIndexForItemBehaviour;
        nextAvailableIndexForItemBehaviour++;
        _EnsureListSize(index);
        return index;
    }

    public bool _IsItemBehaviourBought(int index)
    {
        _EnsureListSize(index);
        return itemBehaviourStatus[index];
    }

    public void _SetItemBehaviourStatus(int index, bool status)
    {
        _EnsureListSize(index);
        itemBehaviourStatus[index] = status;
    }

    private void _EnsureListSize(int index)
    {
        if (index >= itemBehaviourStatus.Count)
        {
            itemBehaviourStatus.AddRange(new bool[index - itemBehaviourStatus.Count + 1]);
        }
    }
}
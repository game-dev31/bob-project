using UnityEngine;

public class ObjectFader : MonoBehaviour
{
    public float fadeSpeed = 10f;
    public float fadeAmount = 0.2f;
    private float originalOpacity;
    private Material[] mats;
    public bool DoFade = false;
    
    void Start()
    {
        mats = GetComponent<Renderer>().materials;
        UpdateOriginalOpacity();
    }

    void Update()
    {
        if (DoFade)
        {
            FadeNow();
        }
        else
        {
            ResetFade();
        }
    }

    void FadeNow()
    {
        foreach (Material mat in mats)
        {
            Color currentColor = mat.color;
            Color smoothColor = new Color(currentColor.r, currentColor.g, currentColor.b, Mathf.Lerp(currentColor.a, fadeAmount, fadeSpeed * Time.deltaTime));
            mat.color = smoothColor;
        }
    }

    void ResetFade()
    {
        foreach (Material mat in mats)
        {
            Color currentColor = mat.color;
            Color smoothColor = new Color(currentColor.r, currentColor.g, currentColor.b,
                Mathf.Lerp(currentColor.a, originalOpacity, fadeSpeed * Time.deltaTime));
            mat.color = smoothColor;
        }
    }

    public void UpdateOriginalOpacity(float opacity = -1f)
    {
        if (mats == null || mats.Length == 0)
        {
            mats = GetComponent<Renderer>().materials;
        }

        if (opacity >= 0)
        {
            originalOpacity = opacity;
        }
        else if (mats.Length > 0)
        {
            originalOpacity = mats[0].color.a;
        }
    }
}
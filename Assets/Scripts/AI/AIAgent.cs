using UnityEngine;
using UnityEngine.AI;
/*
public class AIAgent : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private Transform queuePosition;
    [SerializeField] private bool inQueue = false;
    private ItemBehaviour targetShop;
    private QueueController queueController;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.radius = 0.5f; 
        navMeshAgent.avoidancePriority = Random.Range(0, 99); 
        navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance; 
        inQueue = false;

        queueController = FindObjectOfType<QueueController>();
    }

    void Update()
    {
        if (inQueue && queuePosition != null)
        {
            navMeshAgent.SetDestination(queuePosition.position);
        }
    }

    public void AssignQueuePosition(Transform position, ItemBehaviour shop)
    {
        queuePosition = position;
        targetShop = shop;
        inQueue = true;
    }

    public void EnterShop()
    {
        inQueue = false;
        Debug.Log("Entering the shop.");
    
        if (targetShop != null)
        {
            float calculatedReturn = targetShop.CalculateReturns(targetShop.level);
            Debug.Log($"AI generated {calculatedReturn} coins from shop {targetShop.itemIndex}");
        }
        else
        {
            Debug.LogError("No target shop assigned to AI");
        }

        queueController.RemoveAgent(this);
        
        Destroy(gameObject);
    }


    private void OnDestroy()
    {
        if (queueController != null)
        {
            queueController.RemoveAgent(this);
        }
    }
}*/
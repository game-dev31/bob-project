using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class UpgradeManager : MonoBehaviour
{
    public GameObject upgradeWindow;
    public GameObject itemWindowPrefab;
    private GameObject[] itemWindow;
    private ItemBehaviour[] buildings; 
    public RectTransform scrollContent;
    public int numberOfItems;
    private ItemBehaviour selectedBuilding;
    private bool isHovering = false;
    private float lastClickTime = 0f;
    private float doubleClickTime = 0.3f;
    private GameObject lastClickedObject = null;

    // Hover
    public Color highlightColor = Color.yellow;
    private GameObject highlightedObject;
    private Dictionary<Renderer, Color[]> originalMaterials = new Dictionary<Renderer, Color[]>();

    private Dictionary<int, ItemBehaviour> buildingDict = new Dictionary<int, ItemBehaviour>();

    void Start()
    {
        UpdateBuildings();
        InitializeItemWindows();
        AssignButtonListeners();
        upgradeWindow.SetActive(false);
    }

    void Update()
    {
        UpdateBuildings();

        if (itemWindow.Length != numberOfItems)
        {
            UpdateItemWindows();
            AssignButtonListeners();
        }

        HandleHoverHighlight();
        HandleClick();
    }

    void UpdateBuildings()
    {
        buildings = FindObjectsOfType<ItemBehaviour>();
        numberOfItems = buildings.Length;
        buildingDict.Clear();
        for (int i = 0; i < buildings.Length; i++)
        {
            buildingDict[i] = buildings[i];
        }
    }

    void InitializeItemWindows()
    {
        itemWindow = new GameObject[numberOfItems];

        for (int i = 0; i < itemWindow.Length; i++)
        {
            itemWindow[i] = Instantiate(itemWindowPrefab, scrollContent);
            itemWindow[i].name = "ItemWindow_" + i;

            RectTransform rectTransform = itemWindow[i].GetComponent<RectTransform>();
            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = new Vector2(0, -i * 110);
                //rectTransform.sizeDelta = new Vector2(200, 100);
            }

            SetItemWindowContent(i);
        }

        UpdateScrollContentSize();
    }

    void UpdateItemWindows()
    {
        GameObject[] newItemWindowArray = new GameObject[numberOfItems];

        for (int i = 0; i < Mathf.Min(itemWindow.Length, numberOfItems); i++)
        {
            newItemWindowArray[i] = itemWindow[i];
        }

        for (int i = itemWindow.Length; i < numberOfItems; i++)
        {
            GameObject newItemWindow = Instantiate(itemWindowPrefab, scrollContent);
            newItemWindow.name = "ItemWindow_" + i;

            RectTransform rectTransform = newItemWindow.GetComponent<RectTransform>();
            if (rectTransform != null)
            {
                rectTransform.anchoredPosition = new Vector2(0, -i * 110);
                //rectTransform.sizeDelta = new Vector2(200, 100);
            }

            newItemWindow.SetActive(true);
            newItemWindowArray[i] = newItemWindow;
        }

        for (int i = numberOfItems; i < itemWindow.Length; i++)
        {
            if (itemWindow[i] != null)
            {
                Destroy(itemWindow[i]);
            }
        }

        itemWindow = newItemWindowArray;

        for (int i = 0; i < numberOfItems; i++)
        {
            if (itemWindow[i] != null)
            {
                SetItemWindowContent(i);
            }
        }

        UpdateScrollContentSize();
    }

    void UpdateScrollContentSize()
    {
        float contentHeight = numberOfItems * 110;
        scrollContent.sizeDelta = new Vector2(scrollContent.sizeDelta.x, contentHeight);
    }

    void SetItemWindowContent(int index)
    {
        if (itemWindow[index] != null && buildingDict.TryGetValue(index, out ItemBehaviour building))
        {
            TextMeshProUGUI itemNameText = itemWindow[index].transform.GetChild(1).GetComponentInChildren<TextMeshProUGUI>();
            if (itemNameText != null)
            {
                itemNameText.text = building.buildingName;
            }

            TextMeshProUGUI buildingLevelTxt = itemWindow[index].transform.GetChild(2).GetComponent<TextMeshProUGUI>();
            if (buildingLevelTxt != null)
            {
                if (building.level < 3)
                {
                    int nextLevel = Mathf.Min(building.level + 1, building.maxLevel);
                    buildingLevelTxt.text = $"Level: {building.level} -> {nextLevel}";
                }
                else if(building.level >= 3)
                {
                    buildingLevelTxt.text = "Level Max";
                }
            }

            TextMeshProUGUI upgradeCostText = itemWindow[index].transform.GetChild(3).GetComponent<TextMeshProUGUI>();
            if (upgradeCostText != null)
            {
                if (building.level < 3)
                {
                    int nextLevelCost = building.GetNextUpgradeCost();
                    upgradeCostText.text = $"Cost: {nextLevelCost}";
                }
                else
                {
                    upgradeCostText.text = " ";
                }
            }

            UpdateUpgradeButtonState(index);
        }
    }

    void UpdateUpgradeButtonState(int index)
    {
        if (buildingDict.TryGetValue(index, out ItemBehaviour building))
        {
            Button upgradeButton = itemWindow[index].GetComponentInChildren<Button>();
            if (upgradeButton != null)
            {
                upgradeButton.interactable = building.level < building.maxLevel;
                upgradeButton.onClick.RemoveAllListeners();
                upgradeButton.onClick.AddListener(() => OnUpgradeButtonPressed(index));
            }
        }
    }

    void ShowUpgradeWindow(ItemBehaviour building)
    {
        if (building == null)
        {
            Debug.LogError("Building is null.");
            return;
        }

        upgradeWindow.SetActive(true);

        for (int i = 0; i < itemWindow.Length; i++)
        {
            SetItemWindowContent(i);
            itemWindow[i].SetActive(true);
        }

        UpdateScrollContentSize();
    }

    void AssignButtonListeners()
    {
        for (int i = 0; i < itemWindow.Length; i++)
        {
            UpdateUpgradeButtonState(i);
        }
    }

    public void OnUpgradeButtonPressed(int buttonIndex)
    {
        if (buildingDict.TryGetValue(buttonIndex, out ItemBehaviour building))
        {
            Debug.Log($"Attempting to upgrade building: {building.buildingName}");

            if (building.level < building.maxLevel && building.Upgrade_())
            {
                Debug.Log($"Successfully upgraded {building.buildingName} to level {building.level}");
                UpdateUpgradeButtonState(buttonIndex);
                SetItemWindowContent(buttonIndex);
                Debug.Log($"New upgrade cost for {building.buildingName}: {building.GetNextUpgradeCost()}");
            }
            else
            {
                Debug.Log($"Failed to upgrade {building.buildingName}. Current level: {building.level}, Max level: {building.maxLevel}");
            }
        }
        else
        {
            Debug.LogError($"No building found for index: {buttonIndex}");
        }
    }

    void HandleHoverHighlight()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray);
        GameObject newHighlightedObject = null;

        System.Array.Sort(hits, (x, y) => x.distance.CompareTo(y.distance));

        foreach (RaycastHit hit in hits)
        {
            Transform currentTransform = hit.transform;
            while (currentTransform != null)
            {
                if (currentTransform.CompareTag("isHover"))
                {
                    newHighlightedObject = currentTransform.gameObject;
                    selectedBuilding = currentTransform.GetComponentInParent<ItemBehaviour>();
                    break;
                }
                currentTransform = currentTransform.parent;
            }
            if (newHighlightedObject != null) break;
        }

        if (newHighlightedObject != highlightedObject)
        {
            RestoreOriginalColors();

            if (newHighlightedObject != null)
            {
                HighlightObjectRecursively(newHighlightedObject);
            }

            highlightedObject = newHighlightedObject;
        }

        isHovering = (newHighlightedObject != null);
    }

    void HighlightObjectRecursively(GameObject obj)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer != null)
        {
            Color[] originalColors = new Color[renderer.materials.Length];
            for (int i = 0; i < renderer.materials.Length; i++)
            {
                originalColors[i] = renderer.materials[i].color;
                renderer.materials[i].color = highlightColor;
            }
            originalMaterials[renderer] = originalColors;
        }

        foreach (Transform child in obj.transform)
        {
            HighlightObjectRecursively(child.gameObject);
        }
    }

    private void RestoreOriginalColors()
    {
        if (highlightedObject != null)
        {
            RestoreOriginalColorsRecursively(highlightedObject);
            originalMaterials.Clear();
        }
    }

    void RestoreOriginalColorsRecursively(GameObject obj)
    {
        Renderer renderer = obj.GetComponent<Renderer>();
        if (renderer != null && originalMaterials.TryGetValue(renderer, out Color[] colors))
        {
            for (int i = 0; i < renderer.materials.Length; i++)
            {
                renderer.materials[i].color = colors[i];
            }
        }

        foreach (Transform child in obj.transform)
        {
            RestoreOriginalColorsRecursively(child.gameObject);
        }
    }

    void HandleClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            if (isHovering && selectedBuilding != null)
            {
                float currentTime = Time.time;
                if (currentTime - lastClickTime < doubleClickTime && selectedBuilding.gameObject == lastClickedObject)
                {
                    Debug.Log("Double-clicked on building: " + selectedBuilding.name);
                    ShowUpgradeWindow(selectedBuilding);
                    SFX.singleton.Click();
                }

                lastClickTime = currentTime;
                lastClickedObject = selectedBuilding.gameObject;
            }
            else
            {
                upgradeWindow.SetActive(false);
                lastClickedObject = null;
            }
        }
    }
}
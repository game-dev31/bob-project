using UnityEngine;
using UnityEngine.UI;

public class ButtonHighlight : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private GameObject panel;

    public Color activeColor = new Color(152f / 255f, 152f / 255f, 152f / 255f);
    public Color normalColor = new Color(255, 255, 255);

    void Update()
    {
        if (button != null && panel != null)
        {
            if (panel.activeSelf)
            {
                SetButtonColor(activeColor);
            }
            else
            {
                SetButtonColor(normalColor);
            }
        }
    }

    void SetButtonColor(Color color)
    {
        ColorBlock colors = button.colors;
        colors.normalColor = color;
        colors.highlightedColor = color;
        colors.pressedColor = color;
        colors.disabledColor = color;
        button.colors = colors;
    }
}
using Cinemachine;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public CinemachineVirtualCamera virtualCamera1;
    public CinemachineVirtualCamera virtualCamera2;

    public bool isSwitched = false;
    
    void Start()
    {
        SwitchToCamera1();
        isSwitched = false;
    }

    void Update()
    {
        if (isSwitched == true)
        {
            SwitchToCamera2();
        }
        else
        {
            SwitchToCamera1();
        }
    }

    void SwitchToCamera1()
    {
        virtualCamera1.Priority = 10;
        virtualCamera2.Priority = 0;
    }

    void SwitchToCamera2()
    {
        virtualCamera1.Priority = 0;
        virtualCamera2.Priority = 10;
    }

    public void Switching()
    {
        isSwitched = true;
    }
}
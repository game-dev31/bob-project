﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]

public class ProgressBarCircle : MonoBehaviour {
    [Header("Title Setting")]
    public string Title;
    public Color TitleColor;
    public Font TitleFont;    

    [Header("Bar Setting")]
    public Color BarColor;
    public Color BarBackGroundColor;
    public Color MaskColor;
    public Sprite BarBackGroundSprite;
    [Range(1f, 100f)]
    public int Alert = 20;
    public Color BarAlertColor;

    [Header("Sound Alert")]
    public AudioClip sound;
    public bool repeat = false;
    public float RepearRate = 1f;

    private Image bar, barBackground,Mask;
    private float nextPlay;
    private AudioSource audiosource;
    private Text txtTitle;
    private float barValue;

    private bool isInitialized = false;
    
    private void OnEnable()
    {
        UpdateValue(barValue);
    }

    public float BarValue
    {
        get { return barValue; }

        set
        {
            value = Mathf.Clamp(value, 0, 5);
            barValue = value;
            UpdateValue(barValue);

        }
    }

    private void Awake()
    {
        Transform textTransform = transform.Find("Text");
        if (textTransform != null)
            txtTitle = textTransform.GetComponent<Text>();

        Transform bgTransform = transform.Find("BarBackgroundCircle");
        if (bgTransform != null)
            barBackground = bgTransform.GetComponent<Image>();

        Transform barTransform = transform.Find("BarCircle");
        if (barTransform != null)
            bar = barTransform.GetComponent<Image>();

        audiosource = GetComponent<AudioSource>();

        Transform maskTransform = transform.Find("Mask");
        if (maskTransform != null)
            Mask = maskTransform.GetComponent<Image>();
    }

    private void Start()
    {
        if (txtTitle != null)
        {
            txtTitle.text = Title;
            txtTitle.color = TitleColor;
            txtTitle.font = TitleFont;
        }

        if (bar != null)
        {
            bar.color = BarColor;
        }

        if (Mask != null)
        {
            Mask.color = MaskColor;
        }

        if (barBackground != null)
        {
            barBackground.color = BarBackGroundColor;
            barBackground.sprite = BarBackGroundSprite;
        }

        barValue = 0;
        isInitialized = true;
    }

    void UpdateValue(float val)
    {
        if (!isInitialized)
        {
            return;
        }
        
        if (bar != null)
        {
            bar.fillAmount = -(val / 5) + 1f;
        }
        else
        {
            return;
        }

        if (txtTitle != null)
        {
            txtTitle.text = Title + " " + val + "%";
        }
        else
        {
            return;
        }

        if (barBackground != null)
        {
            if (Alert >= val)
            {
                barBackground.color = BarAlertColor;
            }
            else
            {
                barBackground.color = BarBackGroundColor;
            }
        }
    }

    private void Update()
    {
        Debug.Log(barValue);
        UpdateValue(Mathf.Round(barValue));
        if (!Application.isPlaying)
        {
           
            UpdateValue(5);
            txtTitle.color = TitleColor;
            txtTitle.font = TitleFont;
            Mask.color = MaskColor;
            bar.color = BarColor;
            barBackground.color = BarBackGroundColor;
            barBackground.sprite = BarBackGroundSprite;
            
        }
        else
        {
            if (Alert >= barValue && Time.time > nextPlay)
            {
                nextPlay = Time.time + RepearRate;
                audiosource.PlayOneShot(sound);
            }
        }

        if (barValue >= 5)
        {
            barValue = 0;
        }
    }
}
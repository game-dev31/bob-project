using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ItemUpgradeV3 : MonoBehaviour
{
    [SerializeField] private GameObject Building;
    [SerializeField] private int price;
    public int itemIndex;

    private int currentItemIndex;
    private BoxCollider parentBoxCollider;

    private void Awake()
    {
        // Get the existing BoxCollider from the parent
        parentBoxCollider = transform.parent.GetComponent<BoxCollider>();
        if (parentBoxCollider == null)
        {
            Debug.LogError("No BoxCollider found on the parent object. Please add one in the Unity Editor.");
        }
        else
        {
            ConfigureBoxCollider();
        }
    }

    private void Start()
    {
        while (GameManagement.singleton.BoughtBuilding.Count <= itemIndex)
        {
            GameManagement.singleton.BoughtBuilding.Add(false);
        }

        currentItemIndex = itemIndex;
    }

    private void ConfigureBoxCollider()
    {
        parentBoxCollider.isTrigger = true;
        Transform childTransform = transform.parent.GetChild(0);
        //parentBoxCollider.center = childTransform.localPosition;
        //parentBoxCollider.size = new Vector3(1, 0.25f, 1);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log($"Collision detected with Player. Item Index: {itemIndex}");

            if (Building.gameObject.activeSelf)
            {
                itemIndex = IndexManager.Instance.GetNextAvailableIndex();
                Debug.Log($"ItemUpgradeV3 assigned index {itemIndex}");
            }

            if (itemIndex < 0 || itemIndex >= GameManagement.singleton.BoughtBuilding.Count)
            {
                Debug.LogError($"Invalid itemIndex: {itemIndex}. Cannot proceed.");
                return;
            }

            if (Coinmanagement.singleton.Coin >= price)
            {
                Coinmanagement.singleton.Coin -= price;
                GameManagement.singleton.BoughtBuilding[itemIndex] = true;

                Building.SetActive(true);
                gameObject.SetActive(false);
                
                // Add Rigidbody component after button stepped on
                var rb = transform.parent.GetComponent<Rigidbody>();
                if (rb == null)
                {
                    rb = transform.parent.AddComponent<Rigidbody>();
                    rb.useGravity = false;
                    rb.isKinematic = true;
                }

                var serviceManager = transform.parent.GetComponent<ServiceManager>();
                if (serviceManager == null)
                {
                    transform.parent.AddComponent<ServiceManager>();
                }
                
                SFX.singleton.StepOnButton();

                Debug.Log(
                    $"Item with index {itemIndex} bought for {price}. Remaining coins: {Coinmanagement.singleton.Coin}");
            }
            else
            {
                Debug.Log("Not enough coins to buy this item.");
            }
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    public static GameManagement singleton;
    public List<bool> BoughtBuilding = new List<bool>();

    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(gameObject);
        }
        
        while (BoughtBuilding.Count < 100)
        {
            BoughtBuilding.Add(false);
        }
    }
}

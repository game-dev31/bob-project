using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public LayerMask wallLayer;
    public float checkRadius = 1f;
    private List<ObjectFader> activeFaders = new List<ObjectFader>();
    private Transform playerTransform;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (playerTransform == null) return;

        // Check for walls between camera and player
        Vector3 dirToPlayer = playerTransform.position - transform.position ;
        float distanceToPlayer = dirToPlayer.magnitude;

        Collider[] wallColliders = Physics.OverlapCapsule(transform.position, playerTransform.position, checkRadius, wallLayer);

        // Fade walls that are between camera and player
        foreach (Collider wallCollider in wallColliders)
        {
            ObjectFader fader = wallCollider.GetComponent<ObjectFader>();
            if (fader != null && !activeFaders.Contains(fader))
            {
                fader.DoFade = true;
                activeFaders.Add(fader);
            }
        }

        // Unfade walls that are no longer between camera and player
        for (int i = activeFaders.Count - 1; i >= 0; i--)
        {
            ObjectFader fader = activeFaders[i];
            if (!wallColliders.Contains(fader.GetComponent<Collider>()))
            {
                fader.DoFade = false;
                activeFaders.RemoveAt(i);
            }
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawner : MonoBehaviour
{
    private const float spawnTime = 5;
    public GameObject AIprefab;
    private float counter;
    private int spawnedCount = 0;
    [SerializeField] private int maxSpawnCount = 0;

    private void Start()
    {
        counter = spawnTime;
    }

    void Update()
    {
        if (spawnedCount < maxSpawnCount)
        {
            counter -= Time.deltaTime;
            if (counter <= 0)
            {
                SpawnAI(AIprefab);
            }
        }
    }

    void SpawnAI(GameObject agent)
    {
        Instantiate(agent, transform.position, Quaternion.identity);
        counter = spawnTime;
        spawnedCount++;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public static SFX singleton;
    
    public AudioSource src;
    public AudioClip stepOnButton, click, collectCoin;

    public bool isStep = false;
    private void Awake()
    {
        singleton = this;
    }
    
    public void StepOnButton()
    {
        src.clip = stepOnButton;
        src.Play();
    }

    public void Click()
    {
        src.clip = click;
        src.Play();
    }

    public void CollectCoin()
    {
        src.clip = collectCoin;
        src.Play();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SFXManager : MonoBehaviour
{
    [SerializeField] private AudioSource sfxSource;
    [SerializeField] Image on;
    [SerializeField] Image off;
    
    [SerializeField] TextMeshProUGUI onText;
    [SerializeField] TextMeshProUGUI offText;
    
    private bool sfxMuted = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("sfxMuted"))
        {
            PlayerPrefs.SetInt("sfxMuted", 0);
        }
        else
        {
            Load();
        }
        
        UpdateButtonIcon();
        sfxSource.mute = sfxMuted;
        //AudioListener.pause = muted;
    }

    private void UpdateButtonIcon()
    {
        if (sfxMuted == false)
        {
            on.enabled = true;
            off.enabled = false;
            
            onText.enabled = true;
            offText.enabled = false;
        }
        else
        {
            on.enabled = false;
            off.enabled = true;
            
            onText.enabled = false;
            offText.enabled = true;
        }
    }
    
    public void OnButtonPress()
    {
        if (sfxMuted == false)
        {
            sfxMuted = true;
            sfxSource.mute = sfxMuted;
            //AudioListener.pause = true;
        }
        else
        {
            sfxMuted = false;
            sfxSource.mute = sfxMuted;
            //AudioListener.pause = false;
        }
        
        Save();
        UpdateButtonIcon();
    }

    private void Load()
    {
        sfxMuted = PlayerPrefs.GetInt("sfxMuted") == 1;
    }

    private void Save()
    {
        PlayerPrefs.SetInt("sfxMuted", sfxMuted ? 1 : 0);
    }
}

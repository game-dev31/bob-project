using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    [SerializeField] private float initialCounter = 3.0f;
    [SerializeField] private float[] returns;
    [SerializeField] private GameObject[] eachStageform;
    private float upgradeTimer;
    public int level = 0;
    public string buildingName;
    public int itemIndex;
    [SerializeField] private GameObject QueueSpawn;

    public int baseUpgradeCost = 100;
    public int upgradeCost;
    
    public int maxLevel = 3;

    [SerializeField] private Vector3 queuePos;
    private ShopQueueManager queueManager;

    public bool Upgrade_()
    {
        if (Coinmanagement.singleton.Coin >= upgradeCost && level < maxLevel)
        {
            level++;
            UpdateUpgradeCost();
            Coinmanagement.singleton.Coin -= upgradeCost;
            return true;
        }
        else
        {
            Debug.Log("Not enough coins to upgrade.");
            return false;
        }
    }
    
    private void OnEnable()
    {
        itemIndex = IndexManager.Instance.GetNextAvailableIndex();
        level = 1;
        Debug.Log($"ItemBehaviour {itemIndex} enabled with initial level: {level}");
    }

    private void Start()
    {
        upgradeTimer = initialCounter;

        eachStageform = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            eachStageform[i] = transform.GetChild(i).gameObject;
        }

        SetStageFormActive(level - 1);
        Debug.Log($"ItemBehaviour {itemIndex} started with {eachStageform.Length} stages.");

        GameObject queueSpawnObj = Instantiate(QueueSpawn, new Vector3(queuePos.x, queuePos.y, queuePos.z), Quaternion.identity, transform);
        queueManager = queueSpawnObj.GetComponent<ShopQueueManager>();
        if (queueManager == null)
        {
            Debug.LogError("ShopQueueManager component not found on QueueSpawn prefab");
        }
        
        UpdateUpgradeCost();
    }

    private void Update()
    {
        upgradeTimer -= Time.deltaTime;

        if (level >= 1 && level <= maxLevel)
        {
            if (upgradeTimer <= 0.0f)
            {
                upgradeTimer = initialCounter;
                //CalculateReturns(returns[level - 1]);
            }
        }
        
        SetStageFormActive(level - 1);
    }

    private void SetStageFormActive(int activeIndex)
    {
        if (activeIndex < 0 || activeIndex >= eachStageform.Length)
        {
            SetStageFormInactive();
            return;
        }

        for (int i = 0; i < eachStageform.Length; i++)
        {
            if (eachStageform[i] != null)
            {
                eachStageform[i].SetActive(i == activeIndex);
            }
        }
    }

    private void SetStageFormInactive()
    {
        foreach (GameObject stageForm in eachStageform)
        {
            if (stageForm != null)
            {
                stageForm.SetActive(false);
            }
        }
    }
    
    public float CalculateReturns(int currentLevel)
    {
        if (currentLevel <= 0 || currentLevel > returns.Length)
        {
            Debug.LogError($"Invalid level: {currentLevel}");
            return 0;
        }
        
        float returnValue = returns[currentLevel - 1];
        //float randomFactor = Random.Range(0.1f, 1f);
        //float calculatedReturn = Mathf.Round(returnValue * randomFactor);
        float calculatedReturn = returnValue;
        Coinmanagement.singleton.keepCoin += calculatedReturn;
        Debug.Log($"Calculated return: {calculatedReturn} for itemIndex {itemIndex} at level {currentLevel}");
        return calculatedReturn;
    }
    
    public int GetNextUpgradeCost()
    {
        if (level >= maxLevel)
        {
            return 0;
        }
        return baseUpgradeCost + ((level + 1) * 100);
    }
    
    void UpdateUpgradeCost()
    {
        upgradeCost = baseUpgradeCost + (level * 100);
    }

    // New methods to work with EnhancedAIAgent and ShopQueueManager

    public Transform GetQueuePosition()
    {
        if (queueManager != null)
        {
            return queueManager.GetNextQueuePosition();
        }
        Debug.LogWarning("QueueManager is null, returning transform.position");
        return transform;
    }

    public ShopQueueManager GetQueueManager()
    {
        return queueManager;
    }
}
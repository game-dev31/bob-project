using UnityEngine;

public class ClickFeedback : MonoBehaviour
{
    //public AudioClip clickSound; // Assign this in the Inspector
    public GameObject clickEffect; // Assign a prefab for visual feedback

    private AudioSource audioSource;
    private Camera mainCamera;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Left mouse button
        {
            Vector3 clickPosition = GetClickPosition();
            PlayClickFeedback(clickPosition);
        }
    }

    Vector3 GetClickPosition()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10; // Set this to an appropriate distance from the camera
        return mainCamera.ScreenToWorldPoint(mousePos);
    }

    void PlayClickFeedback(Vector3 position)
    {
        // Play sound
        //if (clickSound != null)
        //{
        //    audioSource.PlayOneShot(clickSound);
        //}

        // Show visual effect
        if (clickEffect != null)
        {
            GameObject effect = Instantiate(clickEffect, position, Quaternion.identity);
            Destroy(effect, 0.5f);
        }
    }
}
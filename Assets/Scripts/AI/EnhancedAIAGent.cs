using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnhancedAIAGent : MonoBehaviour
{
    [Header("Navigation")]
    public string waypointTag = "Waypoint";
    public float detectionRadius = 50f;
    public float waypointWaitTime = 0.5f;
    public float randomWalkRadius = 10f;
    public float randomWalkInterval = 5f;
    public float queueJoinProbability = 0.05f;

    [Header("Movement")]
    public float baseSpeed = 3.5f;
    public float accelerationTime = 0.5f;
    public float decelerationTime = 0.3f;
    public float turnSpeed = 10f;
    public float speedVariation = 0.2f;

    private NavMeshAgent navMeshAgent;
    private Animator animator;
    [SerializeField] private List<GameObject> allWaypoints = new List<GameObject>();
    [SerializeField ]private int currentWaypointIndex = 0;
    private bool hasCompletedWaypoints = false;
    private bool isRandomWalking = false;

    private Transform queuePosition;
    private bool inQueue = false;
    private ItemBehaviour targetShop;
    private QueueController queueController;

    public float currentSpeed;
    private float targetSpeed;
    private Vector3 smoothDampVelocity;

    private enum AIState { Idle, Walking, Queueing }
    private AIState currentState = AIState.Idle;
    private bool isWalking = false;
    
    [Header("Waypoint Handling")]
    public float waypointStoppingDistance = 0.1f;
    public float slowdownDistance = 2f;

    private bool isApproachingWaypoint = false;

    void Start()
    {
        InitializeComponents();
        SetupNavMeshAgent();
        InitializeWaypoints();
    }

    void Update()
    {
        UpdateMovement();
        UpdateBodyOrientation();
        UpdateState();
        CheckDestinationArrival();
    }

    void InitializeComponents()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        queueController = FindObjectOfType<QueueController>();
    }

    void SetupNavMeshAgent()
    {
        navMeshAgent.radius = 0.5f;
        navMeshAgent.speed = baseSpeed;
        navMeshAgent.acceleration = 8;
        navMeshAgent.angularSpeed = 120;
        navMeshAgent.avoidancePriority = Random.Range(0, 99);
        navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
    }

    void InitializeWaypoints()
    {
        allWaypoints = GameObject.FindGameObjectsWithTag(waypointTag).ToList();
        if (allWaypoints.Count > 0)
        {
            ArrangeWaypointsByDistance();
            SetNextWaypoint();
        }
    }

    void UpdateMovement()
    {
        float targetSpeed;

        if (isApproachingWaypoint && navMeshAgent.remainingDistance < slowdownDistance)
        {
            float slowdownFactor = navMeshAgent.remainingDistance / slowdownDistance;
            targetSpeed = baseSpeed * slowdownFactor;
        }
        else
        {
            targetSpeed = baseSpeed + Random.Range(-speedVariation, speedVariation);
        }

        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref smoothDampVelocity.x, 
            navMeshAgent.velocity.magnitude > 0.1f ? accelerationTime : decelerationTime);

        navMeshAgent.speed = currentSpeed;

        bool isCurrentlyWalking = currentSpeed > 0.1f;
        if (isWalking != isCurrentlyWalking)
        {
            isWalking = isCurrentlyWalking;
            UpdateAnimationState();
        }
    }
    
    void UpdateAnimationState()
    {
        if (isWalking)
        {
            animator.SetTrigger("StartWalking");
            animator.ResetTrigger("Idle");
        }
        else
        {
            animator.SetTrigger("Idle");
            animator.ResetTrigger("StartWalking");
        }
    }

    void TransitionToState(AIState newState)
    {
        currentState = newState;
        switch (newState)
        {
            case AIState.Idle:
                if (!isWalking)
                {
                    UpdateAnimationState();
                }
                break;
            case AIState.Walking:
                if (!isWalking)
                {
                    UpdateAnimationState();
                }
                break;
            case AIState.Queueing:
                // Handle queueing state if needed
                break;
        }
    }

    void SetNextWaypoint()
    {
        if (currentWaypointIndex < allWaypoints.Count)
        {
            GameObject currentWaypoint = allWaypoints[currentWaypointIndex];
            navMeshAgent.SetDestination(currentWaypoint.transform.position);
            currentWaypointIndex++;
            isApproachingWaypoint = true;
            navMeshAgent.isStopped = false;
            TransitionToState(AIState.Walking);
        }
        else
        {
            hasCompletedWaypoints = true;
            isRandomWalking = true;
            StartCoroutine(RandomWalkCoroutine());
        }
    }
    void UpdateBodyOrientation()
    {
        if (navMeshAgent.velocity.magnitude > 0.1f)
        {
            Quaternion targetRotation = Quaternion.LookRotation(navMeshAgent.desiredVelocity);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        }
    }

    void UpdateState()
    {
        switch (currentState)
        {
            case AIState.Idle:
                HandleIdleState();
                break;
            case AIState.Walking:
                HandleWalkingState();
                break;
            case AIState.Queueing:
                HandleQueueingState();
                break;
        }
    }

    void HandleIdleState()
    {
        animator.SetTrigger("Idle");
        if (ShouldStartWalking())
        {
            TransitionToState(AIState.Walking);
        }
    }

    void HandleWalkingState()
    {
        if (HasReachedDestination())
        {
            TransitionToState(AIState.Idle);
        }
    }

    void HandleQueueingState()
    {
        if (queueController.IsFirstInQueue(this))
        {
            EnterShop();
        }
    }

   

    bool ShouldStartWalking()
    {
        return !inQueue && (currentWaypointIndex < allWaypoints.Count || isRandomWalking);
    }

    bool HasReachedDestination()
    {
        return !navMeshAgent.pathPending && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance + 0.1f;
    }

    void CheckDestinationArrival()
    {
        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= waypointStoppingDistance)
        {
            if (isApproachingWaypoint)
            {
                StartCoroutine(WaypointReachedCoroutine());
            }
            else if (isRandomWalking)
            {
                StartCoroutine(RandomWalkCoroutine());
            }
        }
    }

    IEnumerator WaypointReachedCoroutine()
    {
        isApproachingWaypoint = false;
        navMeshAgent.isStopped = true;
        TransitionToState(AIState.Idle);

        yield return new WaitForSeconds(waypointWaitTime);

        SetNextWaypoint();
    }

   
    IEnumerator RandomWalkCoroutine()
    {
        if (Random.value < queueJoinProbability)
        {
            FindAndJoinQueue();
        }
        else
        {
            Vector3 randomPosition = GetRandomNavMeshPosition();
            navMeshAgent.SetDestination(randomPosition);
            TransitionToState(AIState.Walking);
        }

        yield return new WaitForSeconds(randomWalkInterval);
        CheckDestinationArrival();
    }

    void ArrangeWaypointsByDistance()
    {
        Vector3 currentPosition = transform.position;
        allWaypoints = allWaypoints.OrderBy(w => Vector3.Distance(currentPosition, w.transform.position)).ToList();
    }

    Vector3 GetRandomNavMeshPosition()
    {
        Vector3 randomDirection = Random.insideUnitSphere * randomWalkRadius;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, randomWalkRadius, 1);
        return hit.position;
    }

    void FindAndJoinQueue()
    {
        ItemBehaviour nearestShop = FindNearestShop();
        if (nearestShop != null)
        {
            AssignQueuePosition(nearestShop.GetQueuePosition(), nearestShop);
        }
    }

    ItemBehaviour FindNearestShop()
    {
        ItemBehaviour[] shops = FindObjectsOfType<ItemBehaviour>();
        return shops.OrderBy(shop => Vector3.Distance(transform.position, shop.transform.position)).FirstOrDefault();
    }

    public void AssignQueuePosition(Transform position, ItemBehaviour shop)
    {
        if (position == null || shop == null)
        {
            Debug.LogError("AssignQueuePosition received a null reference for position or shop.");
            return;
        }
        
        queuePosition = position;
        targetShop = shop;
        inQueue = true;
        navMeshAgent.SetDestination(queuePosition.position);
        TransitionToState(AIState.Queueing);
    }

    public void EnterShop()
    {
        inQueue = false;
        if (targetShop != null)
        {
            float calculatedReturn = targetShop.CalculateReturns(targetShop.level);
            Debug.Log($"AI generated {calculatedReturn} coins from shop {targetShop.itemIndex}");
        }
        else
        {
            Debug.LogError("No target shop assigned to AI");
        }
        
        queueController.RemoveAgent(this);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (queueController != null)
        {
            queueController.RemoveAgent(this);
        }
    }
}
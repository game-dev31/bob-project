using System;
using UnityEngine;

public class CollectCoin : MonoBehaviour
{
    private void Update()
    {
        if (Coinmanagement.singleton.collectCoin == true)
        {
            Coinmanagement.singleton.Coin += Coinmanagement.singleton.keepCoin;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Coinmanagement.singleton.collectCoin = true;
        SFX.singleton.CollectCoin();
    }

    private void OnTriggerExit(Collider other)
    {
        Coinmanagement.singleton.collectCoin = false;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceManager : MonoBehaviour
{
    public ShopQueueManager shopQueue;
    public ProgressBarCircle bar;
    [SerializeField] private GameObject barObj;
    private bool isCounting = false;
    private bool isExited = false;

    private void Update()
    {
        // ShopQueueManager
        Transform childTransform = transform.GetChild(1);
        Transform grandchildTransform = childTransform.GetChild(3);
        shopQueue = grandchildTransform.GetComponent<ShopQueueManager>();
        
        // Bar
        Transform childTf = transform.GetChild(2);
        Transform grandchildtf = childTf.GetChild(0);
        bar = grandchildtf.GetComponent<ProgressBarCircle>();
        barObj = grandchildtf.gameObject;
        
        if (isCounting)
        {
            bar.BarValue += Time.deltaTime;

            if (bar.BarValue >= 5 && barObj.activeSelf)
            {
                barObj.SetActive(false);
                shopQueue.LeaveQueue();
                isCounting = false;
            }
        }
        
        if (shopQueue.GetQueueCount() <= 0.0f || isExited)
        {
            barObj.SetActive(false);
            bar.BarValue = 0;
            isCounting = false;
            isExited = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (barObj != null && shopQueue.GetQueueCount() > 0.0f)
        {
            barObj.SetActive(true);
            bar.BarValue = 0;
            isCounting = true;
        }

        Debug.Log("Enter");
        Debug.Log(shopQueue.GetQueueCount());
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (barObj != null)
        {
            isExited = true;
        }

        Debug.Log("Exit");
    }
}

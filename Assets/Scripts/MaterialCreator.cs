using UnityEngine;

[ExecuteInEditMode]
public class MaterialCreator : MonoBehaviour
{
    [System.Serializable]
    public struct MaterialProperties
    {
        [Range(0f, 1f)]
        public float colorValueR;
        [Range(0f, 1f)]
        public float colorValueG;
        [Range(0f, 1f)]
        public float colorValueB;
        [Range(0f, 1f)]
        public float metallic;
        [Range(0f, 1f)]
        public float smoothness;
    }

    public MaterialProperties properties = new MaterialProperties
    {
        colorValueR = 0.5f, 
        colorValueG = 0.5f,
        colorValueB = 0.5f,
        metallic = 0.5f,
        smoothness = 0.5f
    };

    private Material newMaterial;
    private Renderer rend;
    private ObjectFader objectFader;

    void OnEnable()
    {
        rend = GetComponent<Renderer>();
        if (rend == null)
        {
            Debug.LogWarning("No Renderer component found on this GameObject");
            return;
        }

        objectFader = GetComponent<ObjectFader>();
        CreateMaterial();
    }

    void OnDisable()
    {
        if (Application.isEditor && !Application.isPlaying)
        {
            DestroyImmediate(newMaterial);
        }
        else
        {
            Destroy(newMaterial);
        }
    }

    void CreateMaterial()
    {
        newMaterial = new Material(Shader.Find("Standard"));
        UpdateMaterial();
        if (rend != null)
        {
            rend.sharedMaterial = newMaterial;
        }
    }

    void UpdateMaterial()
    {
        if (newMaterial == null) return;

        
        Color materialColor = new Color(properties.colorValueR, properties.colorValueG, properties.colorValueB);
        if (objectFader != null)
        {
            objectFader.UpdateOriginalOpacity(materialColor.a);
        }
        else
        {
            
        }
        newMaterial.color = materialColor;
        newMaterial.SetFloat("_Metallic", properties.metallic);
        newMaterial.SetFloat("_Glossiness", properties.smoothness);

        
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            UpdateMaterial();
        }
    }

    void OnValidate()
    {
        if (newMaterial != null)
        {
            UpdateMaterial();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class _PlayerMovement : MonoBehaviour
{
    /// public float horizontalInput;
    /// public float verticalInput;
    /// public float turnSpeed = 10;
    Animator animator;
    CharacterController characterController;
    public float speed = 6.0f;
    public float rotationSpeed = 25;
    public float jumpSpeed = 7.5f;
    public float gravity = 20.0f;
    Vector3 inputVec;
    Vector3 targetDirection;
    private Vector3 moveDirection = Vector3.zero;
    
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // // read value from keyboard
        // horizontalInput = Input.GetAxis("Horizontal");
        // verticalInput = Input.GetAxis("Vertical");
        
        // // move the player
        // transform.Translate(Vector3.forward * Time.deltaTime * verticalInput);
        // //transform.Translate(-Vector3.left * Time.deltaTime * horizontalInput);
        // transform.Rotate(Vector3.up * horizontalInput * turnSpeed * Time.deltaTime);

        float x = -(Input.GetAxisRaw("Vertical"));
        float z = Input.GetAxisRaw("Horizontal");
        float y = -(Input.GetAxisRaw("Jump"));
        inputVec = new Vector3(x, y, z);
        
        animator.SetFloat("Input X", z);
        animator.SetFloat("Input Z", -(x));
        animator.SetFloat("Input Y", y);
        
        if ((x != 0 || z != 0) && y == 0)
        {
            animator.SetBool("Moving", true);
            animator.SetBool("Running", true);
            animator.SetBool("Jumping", false);
        }
        else if (y != 0)
        {
            animator.SetBool("Moving", false);
            animator.SetBool("Running", false); 
            animator.SetBool("Jumping", true);
        }
        else
        {
            animator.SetBool("Moving", false);
            animator.SetBool("Running", false);
            animator.SetBool("Jumping", false);
        }
        
        //Jump
        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;
        }
        if (!characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Jump"), Input.GetAxis("Vertical"));
            moveDirection *= speed;
        }
        characterController.Move(moveDirection * Time.deltaTime);
        UpdateMovement();
    }

    void UpdateMovement()
    {
        Vector3 motion = inputVec;
        motion *= (Mathf.Abs(inputVec.x) == 1 && Mathf.Abs(inputVec.z) == 1) ? .7f : 1;
        RotateTowardMovementDirection();
        getCameraRelative();
    }

    void RotateTowardMovementDirection()
    {
        if (inputVec != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDirection), Time.deltaTime * rotationSpeed);
        }
    }

    void getCameraRelative()
    {
        Transform cameraTransform = Camera.main.transform;
        Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
        //forward.y = 0;
        forward = forward.normalized;

        Vector3 right = new Vector3(forward.z, forward.y, -forward.x);
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        float j = Input.GetAxisRaw("Jump");
        targetDirection = (h * right) + (v * forward) /* + (j * forward) */;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoneyMaker : MonoBehaviour
{
    [SerializeField] private Transform[] MoneyPlace = new Transform[9];
    [SerializeField] private GameObject moneyPrefab;
    public float moneyDeliveryTime, YAxis;
    [SerializeField] private bool canMakeMoney = false;

    private float timeSinceLastMoney;
    public int countMoney;
    private int mn_index = 0;
    public float currentYAxis = 0.0f;

    void Start()
    {
        for (int i = 0; i < MoneyPlace.Length; i++)
        {
            MoneyPlace[i] = transform.GetChild(0).GetChild(i);
        }

        timeSinceLastMoney = moneyDeliveryTime;
    }

    private void Update()
    {
        canMakeMoney = Coinmanagement.singleton.keepCoin > 0;

        timeSinceLastMoney += Time.deltaTime;

        if (canMakeMoney && timeSinceLastMoney >= moneyDeliveryTime)
        {
            if (countMoney < Coinmanagement.singleton.keepCoin)
            {
                CreateMoney();
                timeSinceLastMoney = 0f;
            }
        }
    }

    private void CreateMoney()
    {
        GameObject Money = Instantiate(moneyPrefab, new Vector3(transform.position.x, -3f, transform.position.z),
            Quaternion.identity, transform.GetChild(1));

        Vector3 targetPosition = new Vector3(
            MoneyPlace[mn_index].position.x,
            MoneyPlace[mn_index].position.y + currentYAxis,
            MoneyPlace[mn_index].position.z
        );

        Money.transform.DOJump(targetPosition, 2f, 1, 0.5f).SetEase(Ease.OutQuad);

        // Add MoneyPickup component to the instantiated money
        Money.AddComponent<MoneyPickup>().Initialize(this);

        mn_index = (mn_index + 1) % MoneyPlace.Length;

        if (mn_index == 0 && countMoney != 0)
        {
            currentYAxis += YAxis;
        }

        countMoney++;
    }

    public void PickupMoney(GameObject moneyObject)
    {
   
        Destroy(moneyObject);
        
    
        countMoney--;
        
       
    }
}

public class MoneyPickup : MonoBehaviour
{
    private MoneyMaker moneyMaker;

    public void Initialize(MoneyMaker maker)
    {
        moneyMaker = maker;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            moneyMaker.PickupMoney(gameObject);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShopQueueManager : MonoBehaviour
{
    public List<Transform> queuePositions = new List<Transform>();
    private Queue<EnhancedAIAGent> queue = new Queue<EnhancedAIAGent>();
    private ItemBehaviour associatedShop;

    private void Start()
    {
        associatedShop = GetComponentInParent<ItemBehaviour>();
        if (associatedShop == null)
        {
            Debug.LogError("No ItemBehaviour found for this ShopQueueManager");
        }
    }

    public void JoinQueue(EnhancedAIAGent agent)
    {
        if (queue.Count < queuePositions.Count)
        {
            if (agent != null)
            {
                queue.Enqueue(agent);
                agent.AssignQueuePosition(queuePositions[queue.Count - 1], associatedShop);
            }
        }
        else
        {
            Debug.Log("Queue is full. Agent cannot join the queue.");
        }
    }

    public void LeaveQueue()
    {
        if (queue.Count > 0)
        {
            EnhancedAIAGent agent = queue.Dequeue();
            agent.EnterShop();
            UpdateQueuePositions();
        }
    }

    private void UpdateQueuePositions()
    {
        int index = 0;
        foreach (EnhancedAIAGent agent in queue)
        {
            agent.AssignQueuePosition(queuePositions[index], associatedShop);
            index++;
        }
    }
    
    public int GetQueueCount()
    {
        return queue.Count;
    }

    // New methods to make it compatible with QueueController

    public Transform GetNextQueuePosition()
    {
        if (queue.Count < queuePositions.Count)
        {
            return queuePositions[queue.Count];
        }
        return null;
    }

    public ItemBehaviour GetShop()
    {
        return associatedShop;
    }

    public void RemoveFromQueue(EnhancedAIAGent agent)
    {
        if (queue.Contains(agent))
        {
            Queue<EnhancedAIAGent> tempQueue = new Queue<EnhancedAIAGent>();
            while (queue.Count > 0)
            {
                EnhancedAIAGent currentAgent = queue.Dequeue();
                if (currentAgent != agent)
                {
                    tempQueue.Enqueue(currentAgent);
                }
            }
            queue = tempQueue;
            UpdateQueuePositions();
        }
    }
}
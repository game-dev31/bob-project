using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
/*
public class AnimationControllerAI : MonoBehaviour
{
    [Header("Navigation")]
    public string waypointTag = "Waypoint";
    public float detectionRadius = 50f;
    public float waypointWaitTime = 0.5f;
    public float randomWalkRadius = 10f;
    public float randomWalkInterval = 5f;
    public float queueJoinProbability = 0.05f;

    [Header("Collision")]
    public Vector3 collisionBoxSize = new Vector3(1f, 2f, 1f);
    public float evadeDistance = 2f;

    [Header("Movement")]
    public float baseSpeed = 3.5f;
    public float accelerationTime = 0.5f;
    public float decelerationTime = 0.3f;
    public float turnSpeed = 10f;
    public float speedVariation = 0.2f;

    private NavMeshAgent m_NavmeshAgent;
    private Animator animator;
    private List<GameObject> allWaypoints = new List<GameObject>();
    private int currentWaypointIndex = 0;
    private bool hasCompletedWaypoints = false;
    private bool isRandomWalking = false;

    private Transform queuePosition;
    private bool inQueue = false;
    private ItemBehaviour targetShop;
    private QueueController queueController;
    private BoxCollider collisionBox;

    private float currentSpeed;
    private float targetSpeed;
    private Vector3 smoothDampVelocity;

    private enum AIState { Idle, Walking, Queueing }
    private AIState currentState = AIState.Idle;

    void Start()
    {
        InitializeComponents();
        SetupNavMeshAgent();
        InitializeWaypoints();
        AddCollisionBox();
    }

    void Update()
    {
        UpdateMovement();
        UpdateBodyOrientation();
        UpdateState();
        CheckDestinationArrival();
    }

    void InitializeComponents()
    {
        m_NavmeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        queueController = FindObjectOfType<QueueController>();
    }

    void SetupNavMeshAgent()
    {
        m_NavmeshAgent.acceleration = 8;
        m_NavmeshAgent.angularSpeed = 120;
        m_NavmeshAgent.radius = 0.5f;
        m_NavmeshAgent.speed = baseSpeed;
        m_NavmeshAgent.avoidancePriority = Random.Range(0, 99);
        m_NavmeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
    }

    void InitializeWaypoints()
    {
        allWaypoints = GameObject.FindGameObjectsWithTag(waypointTag).ToList();
        if (allWaypoints.Count > 0)
        {
            ArrangeWaypointsByDistance();
            SetNextWaypoint();
        }
    }

    void AddCollisionBox()
    {
        collisionBox = gameObject.AddComponent<BoxCollider>();
        collisionBox.size = collisionBoxSize;
        collisionBox.isTrigger = true;
    }

    void UpdateMovement()
    {
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref smoothDampVelocity.x, 
            m_NavmeshAgent.velocity.magnitude > 0.1f ? accelerationTime : decelerationTime);

        if (m_NavmeshAgent.pathStatus == NavMeshPathStatus.PathPartial)
        {
            targetSpeed = baseSpeed * 0.5f;
        }
        else if (m_NavmeshAgent.pathStatus == NavMeshPathStatus.PathComplete)
        {
            targetSpeed = baseSpeed + Random.Range(-speedVariation, speedVariation);
        }

        m_NavmeshAgent.speed = currentSpeed;

        animator.SetFloat("Speed", currentSpeed / baseSpeed);
        animator.SetBool("IsWalking", currentSpeed > 0.1f);
    }

    void UpdateBodyOrientation()
    {
        if (m_NavmeshAgent.velocity.magnitude > 0.1f)
        {
            Quaternion targetRotation = Quaternion.LookRotation(m_NavmeshAgent.desiredVelocity);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
        }
    }

    void UpdateState()
    {
        switch (currentState)
        {
            case AIState.Idle:
                HandleIdleState();
                break;
            case AIState.Walking:
                HandleWalkingState();
                break;
            case AIState.Queueing:
                HandleQueueingState();
                break;
        }
    }

    void HandleIdleState()
    {
        animator.SetTrigger("Idle");
        if (ShouldStartWalking())
        {
            TransitionToState(AIState.Walking);
        }
    }

    void HandleWalkingState()
    {
        if (HasReachedDestination())
        {
            TransitionToState(AIState.Idle);
        }
    }

    void HandleQueueingState()
    {
        if (IsQueueComplete())
        {
            TransitionToState(AIState.Walking);
        }
    }

    void TransitionToState(AIState newState)
    {
        currentState = newState;
        switch (newState)
        {
            case AIState.Idle:
                animator.SetTrigger("Idle");
                break;
            case AIState.Walking:
                animator.SetTrigger("StartWalking");
                break;
            case AIState.Queueing:
                animator.SetTrigger("StartQueueing");
                break;
        }
    }

    bool ShouldStartWalking()
    {
        return !inQueue && (currentWaypointIndex < allWaypoints.Count || isRandomWalking);
    }

    bool HasReachedDestination()
    {
        return !m_NavmeshAgent.pathPending && m_NavmeshAgent.remainingDistance <= m_NavmeshAgent.stoppingDistance + 0.1f;
    }

    bool IsQueueComplete()
    {
        return inQueue && queueController.IsFirstInQueue(this);
    }

    void CheckDestinationArrival()
    {
        if (HasReachedDestination())
        {
            if (isRandomWalking)
            {
                StartCoroutine(RandomWalkCoroutine());
            }
            else if (currentWaypointIndex < allWaypoints.Count)
            {
                StartCoroutine(WaypointReachedCoroutine());
            }
        }
    }

    IEnumerator WaypointReachedCoroutine()
    {
        TransitionToState(AIState.Idle);
        yield return new WaitForSeconds(waypointWaitTime);
        SetNextWaypoint();
    }

    void SetNextWaypoint()
    {
        if (currentWaypointIndex < allWaypoints.Count)
        {
            GameObject currentWaypoint = allWaypoints[currentWaypointIndex];
            m_NavmeshAgent.SetDestination(currentWaypoint.transform.position);
            currentWaypointIndex++;
            TransitionToState(AIState.Walking);
        }
        else
        {
            hasCompletedWaypoints = true;
            isRandomWalking = true;
            StartCoroutine(RandomWalkCoroutine());
        }
    }

    IEnumerator RandomWalkCoroutine()
    {
        if (Random.value < queueJoinProbability)
        {
            FindAndJoinQueue();
        }
        else
        {
            Vector3 randomPosition = GetRandomNavMeshPosition();
            m_NavmeshAgent.SetDestination(randomPosition);
            TransitionToState(AIState.Walking);
        }

        yield return new WaitForSeconds(randomWalkInterval);
        CheckDestinationArrival();
    }

    void ArrangeWaypointsByDistance()
    {
        Vector3 currentPosition = transform.position;
        allWaypoints = allWaypoints.OrderBy(w => Vector3.Distance(currentPosition, w.transform.position)).ToList();

        for (int i = 1; i < allWaypoints.Count; i++)
        {
            currentPosition = allWaypoints[i - 1].transform.position;
            int nearestIndex = i;
            float nearestDistance = Vector3.Distance(currentPosition, allWaypoints[i].transform.position);

            for (int j = i + 1; j < allWaypoints.Count; j++)
            {
                float distance = Vector3.Distance(currentPosition, allWaypoints[j].transform.position);
                if (distance < nearestDistance)
                {
                    nearestIndex = j;
                    nearestDistance = distance;
                }
            }

            if (nearestIndex != i)
            {
                GameObject temp = allWaypoints[i];
                allWaypoints[i] = allWaypoints[nearestIndex];
                allWaypoints[nearestIndex] = temp;
            }
        }
    }

    Vector3 GetRandomNavMeshPosition()
    {
        Vector3 randomDirection = Random.insideUnitSphere * randomWalkRadius;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, randomWalkRadius, 1);
        return hit.position;
    }

    void FindAndJoinQueue()
    {
        ItemBehaviour nearestShop = FindNearestShop();
        if (nearestShop != null)
        {
            AssignQueuePosition(nearestShop.GetQueuePosition(), nearestShop);
        }
    }

    ItemBehaviour FindNearestShop()
    {
        ItemBehaviour[] shops = FindObjectsOfType<ItemBehaviour>();
        return shops.OrderBy(shop => Vector3.Distance(transform.position, shop.transform.position)).FirstOrDefault();
    }

    public void AssignQueuePosition(Transform position, ItemBehaviour shop)
    {
        if (position == null || shop == null)
        {
            Debug.LogError("AssignQueuePosition received a null reference for position or shop.");
            return;
        }
        
        queuePosition = position;
        targetShop = shop;
        inQueue = true;
        m_NavmeshAgent.SetDestination(queuePosition.position);
        TransitionToState(AIState.Queueing);
    }

    public void EnterShop()
    {
        inQueue = false;
        if (targetShop != null)
        {
            float calculatedReturn = targetShop.CalculateReturns(targetShop.level);
            Debug.Log($"AI generated {calculatedReturn} coins from shop {targetShop.itemIndex}");
        }
        else
        {
            Debug.LogError("No target shop assigned to AI");
        }
        
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bound") && hasCompletedWaypoints)
        {
            HandleBoundaryCollision();
        }
        else if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("AI"))
        {
            EvadeCollision(other.transform.position);
        }
    }

    private void HandleBoundaryCollision()
    {
        m_NavmeshAgent.isStopped = true;
        Vector3 moveDirection = transform.position - m_NavmeshAgent.destination;
        moveDirection.Normalize();
        Vector3 newPosition = transform.position + moveDirection * 0.5f;
        m_NavmeshAgent.Warp(newPosition);
        m_NavmeshAgent.isStopped = false;

        Vector3 randomPosition = GetRandomNavMeshPosition();
        m_NavmeshAgent.SetDestination(randomPosition);
    }

    private void EvadeCollision(Vector3 collisionPoint)
    {
        Vector3 evadeDirection = transform.position - collisionPoint;
        evadeDirection.y = 0;
        evadeDirection.Normalize();

        Vector3 evadePosition = transform.position + evadeDirection * evadeDistance;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(evadePosition, out hit, evadeDistance, NavMesh.AllAreas))
        {
            m_NavmeshAgent.SetDestination(hit.position);
        }
    }
}*/
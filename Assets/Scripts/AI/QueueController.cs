using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class QueueController : MonoBehaviour
{
    public ShopQueueManager queueManager;
    [SerializeField] private List<EnhancedAIAGent> aiAgents = new List<EnhancedAIAGent>();
    [SerializeField] private Transform queueEntryPoint;

    private static HashSet<GameObject> globallyProcessedAgents = new HashSet<GameObject>();

    private void Update()
    {
        EnhancedAIAGent[] currentAgents = FindObjectsOfType<EnhancedAIAGent>();
        queueEntryPoint = transform.GetChild(0);

        List<EnhancedAIAGent> unprocessedAgents = currentAgents
            .Where(agent => !globallyProcessedAgents.Contains(agent.gameObject))
            .ToList();

        unprocessedAgents = unprocessedAgents
            .OrderBy(agent => Vector3.Distance(agent.transform.position, queueEntryPoint.position))
            .ToList();

        foreach (EnhancedAIAGent agent in unprocessedAgents)
        {
            if (queueManager.GetQueueCount() < 3)
            {
                aiAgents.Add(agent);
                globallyProcessedAgents.Add(agent.gameObject);
                AssignAgentToQueue(agent);
            }
        }

        // Clean up null entries in aiAgents list
        aiAgents.RemoveAll(agent => agent == null);

        // Check if the first agent in the queue should enter the shop
        if (aiAgents.Count > 0 && IsFirstInQueue(aiAgents[0]))
        {
            aiAgents[0].EnterShop();
            RemoveAgent(aiAgents[0]);
        }
    }

    private void AssignAgentToQueue(EnhancedAIAGent agent)
    {
        Transform queuePosition = queueManager.GetNextQueuePosition();
        if (queuePosition != null)
        {
            agent.AssignQueuePosition(queuePosition, queueManager.GetShop());
        }
    }

    public void RemoveAgent(EnhancedAIAGent agent)
    {
        aiAgents.Remove(agent);
        globallyProcessedAgents.Remove(agent.gameObject);
        queueManager.RemoveFromQueue(agent);
    }

    public bool IsFirstInQueue(EnhancedAIAGent agent)
    {
        return aiAgents.Count > 0 && aiAgents[0] == agent;
    }
}
using UnityEngine;

public class UIFaceCamera : MonoBehaviour
{
    private Camera mainCamera;

    void Start()
    {
        // Find the main camera in the scene
        mainCamera = Camera.main;

        // If no main camera is found, log an error
        if (mainCamera == null)
        {
            Debug.LogError("No main camera found in the scene!");
        }
    }

    void Update()
    {
        if (mainCamera != null)
        {
            // Make the UI element face the camera
            transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward,
                mainCamera.transform.rotation * Vector3.up);
        }
    }
}